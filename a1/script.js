/*console.log("Hello");*/


//Object literal
let trainer = {};
console.log(trainer);

//Initiliaze/add the object

// Properties
trainer.name = 'Ash Ketchum';
trainer.age = 10;
trainer.pokemon = ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'];
trainer.friends = {
	kanto: ['Brock', 'Misty'],
	hoenn: ['May', 'Max']
};

// Methods
trainer.talk = function() {
	console.log('Pikachu, I choose you!');
};

// Check all of the properties and methods were properly added
console.log(trainer);

console.log("Result of dot notation: ");
console.log(trainer.name);


//square bracket

console.log(trainer["pokemon"]);


//access the trainer talk method
trainer.talk();


//constructor function
// Create a constructor function for creating a pokemon
// Create a constructor function for creating a pokemon
function Pokemon(name, level) {

	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods 
		// Will accept an object as a target
		this.tackle = function(target) {
			console.log(this.name + " tackled " + target.name)
			target.health -= this.attack;
			console.log(target.name + "'s health is now reduced to " + target.health);

			if (target.health <= 0) {
				target.faint()
			}
		};

		this.faint = function() {
			console.log(this.name + " fainted.")
		}
	};


	let pikachu = new Pokemon ('Pikachu', 12);
	console.log(pikachu);

	let geodude = new Pokemon ('Geodude', 8);
	console.log(geodude);

	let mewto = new Pokemon ('Mewto', 100);
	console.log(mewto);


// Invoke the tackle method and target a different object 
geodude.tackle(pikachu);
console.log(pikachu);

//Invoke the tackle method and target a dufferent object
mewto.tackle(geodude);
console.log(geodude);












