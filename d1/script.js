//console.log("Hellow");

// OBJECTS

/*
	-An object is a data type that is used to represent real word objects
	- It is a collection of related data and/or functionalities
	- In javascript, most core JS features like strings, and arrays are objects
	- Information stored in objects are represented in a "key value pair".
	- A "key" is also mostly refereed to as "property" of an object.
	- Different data types may be stored in an object
*/


//Creating objects using object initializer

	
	let cellphone = {
		name : "Nokia",
		manufactureDate : 1999
	};
	console.log(cellphone);
	console.log(typeof cellphone);


function laptop(name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate;
};


let anotherLaptop = new laptop ('Lenovo', 2008);
console.log("Results from creating objects using object constructors");
console.log(anotherLaptop);

let myLaptop = new laptop ('Macbook Air', 2020);
console.log(myLaptop);

let newLaptop = new laptop ('Honda', 2022);
console.log(newLaptop);



//Assessing thr value

//Using dot notation

console.log(myLaptop.name);
console.log(cellphone.name);


//Using square bracket notation

console.log(myLaptop['name']);


//Initializing/Adding/Deleting/Reassigning Object Properties

/*
	-Like any other variable, in JavaScript, objects may have their properties initialized/added after the object was created/declared.
	- This is is useful for time when an object's properties are undetermined at the of creating them.


*/

let car = {} // empty object
console.log(car);

// Initializing/Adding an object properties using dot notation
car.name = "Honda Civic";
console.log(car)


car["manufacture date"] = 2019;
console.log(car["manufacture date"])
console.log(car)



//deleting object properties
delete car["manufacture date"];
console.log(car);


//Reassigning object properties
car.name = 'Dodge Charger RT';
console.log(car);




// Object Methods
/*
	-A method is a function which is a property of an object.
	-They are also function and one of the key differences that they have is that methods are functions related to a specific object.
	
*/



let person = {
	name: 'John',
	talk: function() {
		console.log('Hellow my name is ' + this.name);
	}
}
console.log(person);
person.talk();


//Adding methods to objects

person.walk = function(){
	console.log(this.name + " walk 25 steps forward");
}

person.walk();
console.log(person);


//Methods are useful for creating reuseable function 

let friend = {
	firstName: 'Joe',
	lastName: "Smith",
	address: {
		city: "Austin",
		country: "Texas",
	},
	emails: ['joe@Gmail.com',"joesmitch@gmail.com"],
	introduce: function(){
		console.log("Hello, my name is " + this.firstName + " " + this.lastName);
	}
}

friend.introduce();
console.log(friend.firstName + " " + friend.lastName);
console.log(friend.address.city + friend.address.country)

//Real World Application of Objects
/*
	Scenario: 
	1. We would like to create a game that would have several pokemon interact with each other
	2. Every Pokemon would have the same set of stats, properties, and function

*/

//Using object literals to create multiple kinds of pokemon















